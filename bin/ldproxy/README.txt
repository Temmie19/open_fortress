This is a little shim between Source and our client.so for linux,
which sets up the LD_LIBRARY_PATH to include our bin folder.
Without it, fmod.so can't be found on linux.
the ldproxy.txt file (open_fortress/bin/ldproxy.txt) specifies what libraries to preload.

for more info, see the README in the source repo,
at /ldproxy/README.txt
