WeaponData
{
	// Attributes Base.
	"printname"		"#TF_Weapon_Thundergun"
	"BuiltRightHanded"	"0"
	"weight"		"2"
	"WeaponType"		"primary"
	"ITEM_FLAG_NOITEMPICKUP" 	"1"
		
	// Attributes TF.
	"Damage"		"15"
	"InstagibDamage"	"999"
	"Range"			"4096"
	"BulletsPerShot"	"1"
	"Spread"		"0.04"
	"TimeFireDelay"		"0.15"
	"TimeIdle"		"5.0"
	"TimeIdleEmpty"		"0.25"
	"TimeReload"		"1.05"
	"ProjectileType"	"projectile_bullet"
	"TracerEffect"		"bullet_pistol_tracer01"

	// Ammo & Clip.
	"primary_ammo"		"WEAPON_AMMO"
	"secondary_ammo"	"None"
	clip_size		2
	default_clip		2
	"HasTeamSkins_Viewmodel"			"1"
	"HasTeamSkins_Worldmodel"			"1"
	"MaxAmmo"	"12"

	// bucket.	
	"bucket"			"1"
	"bucket_position"		"0"
	
	// bucket_dms.
	"bucket_dm"		"1"
	"bucket_dm_position"	"6" //NOTE: This is 1 AFTER it's "actual" position. "Fixes" a bug while playing as Spy in DM.

	// Animation.
	"viewmodel"		"models/weapons/v_models/v_flamethrower_pyro.mdl"
	"playermodel"		"models/weapons/w_models/w_flamethrower.mdl"
	"anim_prefix"		"ac"
	
	"CenteredViewmodelOffset_Y" "-11.5"
	"CenteredViewmodelOffset_Z" "-1"
	"CenteredViewmodelAngle_Y" "-3"
	
	"MinViewmodelOffset_X" "10"
	"MinViewmodelOffset_Y" "0"
	"MinViewmodelOffset_Z" "-10"

	// Muzzleflash
	"MuzzleFlashParticleEffect" "muzzle_syringe"

	// Sounds.
	// Max of 16 per category (ie. max 16 "single_shot" sounds).
	SoundData
	{
		"single_shot"	"Weapon_RPG.NPC_Single"
//		"reload"		"Weapon_RPG.WorldReload"
		"burst"			"Weapon_RPG.SingleCrit"	
	}

	// Weapon Sprite data is loaded by the Client DLL.
	TextureData
	{
		"weapon"
		{
				"file"		"sprites/bucket_rl"
				"x"		"0"
				"y"		"0"
				"width"		"200"
				"height"		"128"
		}
		"weapon_s"
		{	
				"file"		"sprites/bucket_rl"
				"x"		"0"
				"y"		"0"
				"width"		"200"
				"height"		"128"
		}
		"ammo"
		{
				"file"		"sprites/a_icons1"
				"x"		"55"
				"y"		"60"
				"width"		"73"
				"height"	"15"
		}
		"crosshair"
		{
				"file"		"sprites/crosshairs"
				"x"		"32"
				"y"		"32"
				"width"		"32"
				"height"	"32"
		}
		"autoaim"
		{
				"file"		"sprites/crosshairs"
				"x"		"0"
				"y"		"48"
				"width"		"24"
				"height"	"24"
		}
	}
}