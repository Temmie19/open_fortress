//
// Team Fortress - Pyro Player Class
//
PlayerClass
{
	// Info.
	"name"				"pyro"
	"localize_name"		"TF_Class_Name_Pyro"
	
	// Models.
	"model"				"models/player/pyro.mdl"
	"arm_model"			"models/weapons/c_models/c_pyro_arms.mdl"
	"bot_model"			"models/bots/pyro/bot_pyro.mdl"
	"alt_model"			"models/player/pyro.mdl"
	"alt_arm_model"		"models/weapons/c_models/c_pyro_arms.mdl"
	
	"ViewVector"		"68"
	
	// These are used in the class selection
	"ClassSelectImageRed"			"class_sel_sm_pyro_red"
	"ClassSelectImageBlue"			"class_sel_sm_pyro_blu"
	"ClassSelectImageMercenary"		"class_sel_sm_pyro_mercenary"
	
	// These are used in your player icon next to your health
	"ClassImageRed"					"../hud/class_pyrored"
	"ClassImageBlue"				"../hud/class_pyroblue"
	"ClassImageMercenary"			"../hud/class_pyromercenary"
	"ClassImageColorless"			"../hud/colorless/class_pyrocolorless"
	
	// Stats.
	"speed_max"			"300"
	"health_max"		"175"
	"armor_max"			"0"

	// Grenades.
	"grenade1"			"TF_GRENADE_NORMAL"
	"grenade2"			"TF_GRENADE_NAPALM"
	"grenade3"			"NONE"

	// Weapons.
	"weapon1"			"TF_WEAPON_FIREAXE"
	"weapon2"			"TF_WEAPON_SHOTGUN"
	"weapon3"			"TF_WEAPON_FLAMETHROWER"
	"weapon4"			"NONE"
	"weapon5"			"NONE"
	"weapon6"			"NONE"
	"weapon7"			"NONE"
	"weapon8"			"NONE"
	"weapon9"			"NONE"
	
	// Buildables.
	"buildable1"		"OBJ_SENTRYGUN"
	"buildable2"		"OBJ_DISPENSER"
	"buildable3"		"OBJ_TELEPORTER"

	// Death Sounds.
	"sound_death"					"Pyro.Death"
	"sound_crit_death"				"Pyro.CritDeath"
	"sound_melee_death"				"Pyro.MeleeDeath"
	"sound_explosion_death"			"Pyro.ExplosionDeath"
	
	// Ammo.
	AmmoMax
	{
		"TF_AMMO_METAL"				"200"
	}
	
	"TFC"
	{
		"model"			"models/player/tfc/pyro_tfc.mdl"
		"arm_model"		"models/weapons/c_models/c_tfc_pyro_arms.mdl"
		
		"speed_max"		"300"
		//"health_max"	"100" //Uncomment this later when armor is implemented
		"health_max"	"175"
		"armor_max"		"150"
		
		"weapon1"		"TFC_WEAPON_CROWBAR"
		"weapon2"		"TFC_WEAPON_SHOTGUN_SB"
		"weapon4"		"TFC_WEAPON_INCENDIARYCANNON"
		"weapon3"		"TFC_WEAPON_FLAMETHROWER"
		"weapon5"		"NONE"
		"weapon6"		"NONE"
		"weapon7"		"NONE"
		"weapon8"		"NONE"
		"weapon9"		"NONE"	
	}
	
	"Zombie"
	{
		"model"		"models/player/zombie/pyro_zombie.mdl"
		"arm_model"	"models/weapons/c_models/c_zombie_pyro_arms.mdl"
		
		// Weapons.
		"weapon1"			"TF_WEAPON_CLAWS"
		"weapon2"			"NONE"
		"weapon3"			"NONE"
		"weapon4"			"NONE"
		"weapon5"			"NONE"
		"weapon6"			"NONE"
		"weapon7"			"NONE"
		"weapon8"			"NONE"
		"weapon9"			"NONE"
	}
}
