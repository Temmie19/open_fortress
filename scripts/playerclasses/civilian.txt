//
// Team Fortress - Civilian Player Class
//
PlayerClass
{
	// Info.
	"name"				"civilian"
	"localize_name"		"TF_Class_Name_Civilian"
	
	// Models.
	"model"				"models/player/civilian.mdl"
	"arm_model"			"models/weapons/c_models/c_civilian_arms.mdl"
	"bot_model"			"models/player/civilian.mdl"
	"alt_model"			"models/player/civilian.mdl"
	"alt_arm_model"		"models/weapons/c_models/c_civilian_arms.mdl"
	
	"ViewVector"			"68"
	
	// These are used in the class selection
	"ClassSelectImageRed"			"class_sel_sm_civilian_red"
	"ClassSelectImageBlue"			"class_sel_sm_civilian_blu"
	"ClassSelectImageMercenary"		"class_sel_sm_civilian_mercenary"
	
	// These are used in your player icon next to your health
	"ClassImageRed"					"../hud/class_civilianred"
	"ClassImageBlue"				"../hud/class_civilianblue"
	"ClassImageMercenary"			"../hud/class_civilianmercenary"
	"ClassImageColorless"			"../hud/colorless/class_civiliancolorless"
	
	// Stats.
	"speed_max"			"280"
	"health_max"		"200"
	"armor_max"			"0"

	// Grenades.
	"grenade1"			"NONE"
	"grenade2"			"NONE"
	"grenade3"			"NONE"

	// Weapons.
	"weapon1"			"TF_WEAPON_UMBRELLA"
	"weapon2"			"NONE"
	"weapon3"			"NONE"
	"weapon4"			"NONE"
	"weapon5"			"NONE"
	"weapon6"			"NONE"
	"weapon7"			"NONE"
	"weapon8"			"NONE"
	"weapon9"			"NONE"
	
	
	// Buildables.
	"buildable1"		"OBJ_SENTRYGUN"
	"buildable2"		"OBJ_DISPENSER"
	"buildable3"		"OBJ_TELEPORTER"
	
	// Death Sounds.
	"sound_death"					"Civilian.Death"
	"sound_crit_death"				"Civilian.CritDeath"
	"sound_melee_death"				"Civilian.MeleeDeath"
	"sound_explosion_death"			"Civilian.ExplosionDeath"	
	
	// Ammo.
	AmmoMax
	{
		"TF_AMMO_METAL"				"200"
	}
	
	"TFC"
	{
		"speed_max"		"300"
		//"health_max"	"50" //Uncomment this later when armor is implemented
		"health_max"	"200"
		"armor_max"		"0"	
		
		"model"			"models/player/civilian.mdl"
		"arm_model"		"models/weapons/c_models/c_civilian_arms.mdl"		
	}
	
	"Zombie"
	{
		"model"		"models/player/zombie/civilian_zombie.mdl"
		"arm_model"	"models/weapons/c_models/c_zombie_civilian_arms.mdl"	
		
		// Weapons.
		"weapon1"			"TF_WEAPON_CLAWS"
		"weapon2"			"NONE"
		"weapon3"			"NONE"
		"weapon4"			"NONE"
		"weapon5"			"NONE"
		"weapon6"			"NONE"
		"weapon7"			"NONE"
		"weapon8"			"NONE"
		"weapon9"			"NONE"
		
	}
}
