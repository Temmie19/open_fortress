//
// Team Fortress - Demoman Player Class
//
PlayerClass
{
	// Info.
	"name"				"demoman"
	"localize_name"		"TF_Class_Name_Demoman"
	
	// Models.
	"model"				"models/player/demo.mdl"
	"arm_model"			"models/weapons/c_models/c_demo_arms.mdl"
	"bot_model"			"models/bots/demo/bot_demo.mdl"
	"alt_model"			"models/player/demo.mdl"
	"alt_arm_model"		"models/weapons/c_models/c_demo_arms.mdl"
	
	"ViewVector"		"68"
	
	// These are used in the class selection
	"ClassSelectImageRed"			"class_sel_sm_demo_red"
	"ClassSelectImageBlue"			"class_sel_sm_demo_blu"
	"ClassSelectImageMercenary"		"class_sel_sm_demo_mercenary"
	
	// These are used in your player icon next to your health
	"ClassImageRed"					"../hud/class_demored"
	"ClassImageBlue"				"../hud/class_demoblue"
	"ClassImageMercenary"			"../hud/class_demomercenary"
	"ClassImageColorless"			"../hud/colorless/class_democolorless"
	
	// Stats.
	"speed_max"			"280"
	"health_max"		"175"
	"armor_max"			"0"

	// Grenades.
	"grenade1"			"TF_GRENADE_NORMAL"
	"grenade2"			"TF_GRENADE_MIRV"
	"grenade3"			"NONE"

	// Weapons.
	"weapon1"			"TF_WEAPON_BOTTLE"
	"weapon2"			"TF_WEAPON_PIPEBOMBLAUNCHER"
	"weapon3"			"TF_WEAPON_GRENADELAUNCHER"
	"weapon4"			"NONE"
	"weapon5"			"NONE"
	"weapon6"			"NONE"
	"weapon7"			"NONE"
	"weapon8"			"NONE"
	"weapon9"			"NONE"
	// Buildables.
	"buildable1"		"OBJ_SENTRYGUN"
	"buildable2"		"OBJ_DISPENSER"
	"buildable3"		"OBJ_TELEPORTER"

	// Death Sounds.
	"sound_death"					"Demoman.Death"
	"sound_crit_death"				"Demoman.CritDeath"
	"sound_melee_death"				"Demoman.MeleeDeath"
	"sound_explosion_death"			"Demoman.ExplosionDeath"	
	
	// Ammo.
	AmmoMax
	{
		"TF_AMMO_METAL"				"200"
	}
	
	"Zombie"
	{
		"model"				"models/player/zombie/demo_zombie.mdl"
		"arm_model"			"models/weapons/c_models/c_zombie_demo_arms.mdl"	
		
		"weapon1"			"TF_WEAPON_CLAWS"
		"weapon2"			"NONE"
		"weapon3"			"NONE"
		"weapon4"			"NONE"
		"weapon5"			"NONE"
		"weapon6"			"NONE"
		"weapon7"			"NONE"
		"weapon8"			"NONE"
		"weapon9"			"NONE"
	}	
	
	"TFC"
	{
		"model"			"models/player/tfc/demo_tfc.mdl"
		"arm_model"		"models/weapons/c_models/c_tfc_demo_arms.mdl"	
	
		"speed_max"		"280"
		//"health_max"	"90" //Uncomment this later when armor is implemented
		"health_max"	"175"
		"armor_max"		"120"
		
		"weapon1"		"TFC_WEAPON_CROWBAR"
		"weapon2"		"TFC_WEAPON_SHOTGUN_SB"
		"weapon4"		"TFC_WEAPON_PIPEBOMBLAUNCHER"
		"weapon3"		"TFC_WEAPON_GRENADELAUNCHER"
		"weapon5"		"NONE"
		"weapon6"		"NONE"
		"weapon7"		"NONE"
		"weapon8"		"NONE"
		"weapon9"		"NONE"
	}
}
